// Vars
var $item1 = $(".item-1");
var $item2 = $(".item-2");

// Reset timeline
// Reset both items to opacity 0 so they're allways
// invisible before starting animation
function resetTL() {
  var timeline = new TimelineMax()
    .set($item1, { opacity: 0 })
    .set($item2, { opacity: 0 });
  return timeline;
}

// Sub timeline 1
function subTL1() {
  var timeline = new TimelineMax()
    .fromTo(
      $item1,
      1,
      { x: -400, opacity: 0 },
      { x: -5, opacity: 1, ease: Expo.easeOut }
    )
    // Use 'overwrite:none' to make sure the animation on the first line is finished
    // when repeating the main timeline.
    // See example of 'broken' timeline without overwrite: https://codepen.io/pieceofcode/pen/wOyPZx
    .to($item1, 2, { x: 0, overwrite: "none" }, "-=0.5");
  return timeline;
}

// Sub timeline 2
function subTL2() {
  var timeline = new TimelineMax()
    .fromTo(
      $item2,
      1,
      { x: 400, opacity: 0 },
      { x: 5, opacity: 1, ease: Expo.easeOut }
    )
    // Use 'overwrite:none' to make sure the animation on the first line is finished
    // when repeating the main timeline.
    // See example of 'broken' timeline without overwrite: https://codepen.io/pieceofcode/pen/wOyPZx
    .to($item2, 2, { x: 0, overwrite: "none" }, "-=0.5");
  return timeline;
}

// Main timeline
var mainTL = new TimelineMax({ repeat: -1 });
mainTL
  .add(resetTL)
  .add(subTL1(), "start")
  .add(subTL2(), "start+=3");
